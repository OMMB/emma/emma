# Emma

## Installation

Make sure to create a `.env` file, formatted like `.env.example`.
Alternatively, add `TOKEN` to your environment variables

## Running

Nodemon and PM2 are used for running Emma.
To run with file watching, run `npm run dev`.
To run for production, run `npm start`, Emma can be killed with `npx pm2 stop "Emma"` or `npx pm2 kill`