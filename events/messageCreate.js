const { Signale } = require("signale");
const chalk = require("chalk");
const logging = require("../modules/logging");

const { messageHandler } = require("../modules/messageHandler");

module.exports = {
	name: "messageCreate",
	once: false,
	async execute(message, client) {
		messageHandler(message.content, message.author, message, client);
	},
};
