const { Signale } = require("signale");
const chalk = require("chalk");
const logging = require("../modules/logging");

module.exports = {
	name: "ready",
	once: true,
	async execute(client) {
		logging.info(
			chalk`{bgBlue {black {bold Ready!}}} Logged in as ${client.user.tag}`
		);
	},
};
