const { Signale } = require("signale");
const chalk = require("chalk");

const options = {
	info: {
		types: {
			command: {
				label: "command",
				color: "yellow",
				badge: "i",
			},
			event: {
				label: "event",
				color: "magenta",
				badge: "i",
			},
		},
	},
};

module.exports = new Signale(options.info);
