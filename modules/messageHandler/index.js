const fs = require("fs");
const logger = require("../logging");

const triggers = require("./triggers.json");

module.exports = {
	messageHandler(message, author, rawMessage, client) {
		let keys = Object.keys(triggers);
		for (let i = 0; i < keys.length; i++) {
			const key = keys[i];
			for (let j = 0; j < triggers[key].length; j++) {
				const element = triggers[key][j];
				if (message.includes(element)) {
					let responses = JSON.parse(
						fs.readFileSync(
							`./modules/messageHandler/responses/${keys[i]}.json`
						)
					);
					if (responses.type == "random") {
						console.log(keys[i]);
						responses.responses[
							Math.floor(Math.random() * responses.responses.length)
						]
							.split("\r")
							.forEach((word) => {
								rawMessage.channel.send(word);
							});
					}
				}
			}
		}
	},
};
