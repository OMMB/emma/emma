const { Client, Intents } = require("discord.js");
const { Routes } = require("discord-api-types/v9");

const logger = require("./modules/logging");
const chalk = require("chalk");

const fs = require("fs");

const dotenv = require("dotenv");

const argv = require("minimist")(process.argv.slice(2), {
	boolean: ["debug"],
	alias: { d: "debug" },
});

if (argv.debug) {
	process.env.DEBUG = true;
}

dotenv.config();

let client = new Client({
	intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES],
});

const eventFiles = fs
	.readdirSync("./events")
	.filter((file) => file.endsWith(".js"));

for (const file of eventFiles) {
	const event = require(`./events/${file}`);
	logger.event(chalk`Loading event {magenta {bold ${event.name}}}`);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(...args, client));
	} else {
		client.on(event.name, (...args) => event.execute(...args, client));
	}
}

client.login(process.env.TOKEN);
